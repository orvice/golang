#!/usr/bin/env bash
set -ex

apt-get update

echo "Install go-swagger"
go install  github.com/go-swagger/go-swagger/cmd/swagger@v0.28.0

echo "Install reviewdog"
curl -sfL https://raw.githubusercontent.com/reviewdog/reviewdog/master/install.sh | sh -s -- -b /usr/bin

echo "Install golangci-lint"
curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b /usr/bin v1.42.0

echo "Install go-junit-report"
go install github.com/jstemmer/go-junit-report@latest

##  Install procto
echo "Install protoc"
apt install -y protobuf-compiler

echo "Install protoc-gen-go"
go install google.golang.org/protobuf/cmd/protoc-gen-go@latest

## gogo
go install github.com/gogo/protobuf/protoc-gen-gogo@master

go install github.com/gogo/protobuf/protoc-gen-gogo@master
go install github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway@master
go install github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger@master
go install github.com/mwitkow/go-proto-validators/protoc-gen-govalidators@master
go install github.com/rakyll/statik@master

echo "Install protoc-gen-gofast"
go install github.com/gogo/protobuf/protoc-gen-gofast@v1.3.2

echo "Install protoc-gen-doc"
go install github.com/pseudomuto/protoc-gen-doc/cmd/protoc-gen-doc@v1.5.0



## Install mysql
echo "Install default-mysql-client"
apt-get install -y default-mysql-client

